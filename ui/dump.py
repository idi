# This is a simple "UI" frontend that will just sweep a file and dump the data.

import sys
import file.file
import file.section

def _comment(seg):
	return seg.comment and (' ; ' + seg.comment) or ''

def _trim(string, length):
	return len(string) > length and (string[:(length - 4)].rstrip() + ' ...') or string

def dump_dummy(f, seg):
	print '[0x%04x] <<< %s >>> (%04d)%s' % (seg.memofs, seg.name, seg.len, _comment(seg))

def dump_code(f, seg):
	print '[0x%04x] CODE     %s%s' % (seg.memofs, seg.txt, _comment(seg))

def dump_data(f, seg):
	data = f.data(seg.physofs, seg.len)
	strs = seg.string(data)
	print '[0x%04x] DATA %-2d  %s%s' % (seg.memofs, seg.cellsize, _trim(', '.join(strs), 72), _comment(seg))

def dump_comment(f, seg):
	print '[0x%04x]' % (seg.memofs)
	print '[0x%04x] ; %s' % (seg.memofs, seg.comment)
	print '[0x%04x]' % (seg.memofs)

def start(dclass):
	if (len(sys.argv) < 2):
		sys.stderr.write('Usage: ' + sys.argv[0] + ' FILE\n')
		sys.exit(1)
	f = file.file.file(sys.argv[1])
	d = dclass(f)
	d.sweep()
	map = f.map

	print 'Sections:'
	for sect in f.sections:
		print '%-016s: %04x (phys %04x) size %04x' % (sect.name, sect.memofs, sect.physofs, sect.size)

	print 'Symbols:'
	for sym in f.symbols.values():
		print '%-020s: %04x @ %-08s (%s)' % (sym.name, sym.memofs, sym.section, sym.type)

	print 'Map:'
	dumpers = { 'seg_code': dump_code, 'seg_data': dump_data, 'seg_comment': dump_comment }

	for seg in map.segments:
		if seg.__class__.__name__ in dumpers:
			dumpers[seg.__class__.__name__](f, seg)
		else:
			dump_dummy(f, seg)
