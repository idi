import gobject
import gtk


(
	SYM_COLUMN_NAME,
	SYM_COLUMN_TYPE,
	SYM_COLUMN_SECT,
	SYM_COLUMN_MEMOFS
) = range(4)


class symview (gtk.Window):
	def __init__(self, mainwin):
		gtk.Window.__init__(self)
		self.main = mainwin
		self.set_screen(mainwin.get_screen())
		self.set_title('IDI: Symbols')

		self.set_default_size(500, 300)

		sw = gtk.ScrolledWindow()
		sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
		sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
		self.add(sw)

		model = self.__create_model()

		treeview = gtk.TreeView(model)
		treeview.set_rules_hint(True)
		treeview.set_search_column(SYM_COLUMN_NAME)

		sw.add(treeview)
		self.__add_columns(treeview)
		self.show_all()

	def __create_model(self):
		# See remarks in ui.gtk.sectview about hex offset
		lstore = gtk.ListStore(
			gobject.TYPE_STRING,
			gobject.TYPE_STRING,
			gobject.TYPE_STRING,
			gobject.TYPE_STRING)

		i = 1
		syms = self.main.file.symbols.values()
		syms.sort(key = (lambda x: x.memofs))
		for sym in syms:
			iter = lstore.append()
			if sym.type == 'object':
				# XXX: Can't get this to work :-((
				#span1 = '<span background="darkgreen">'
				#span2 = '</span>'
				span1 = span2 = ''
			else:
				span1 = span2 = ''
			lstore.set(iter,
				SYM_COLUMN_NAME, span1+sym.name+span2,
				SYM_COLUMN_TYPE, sym.type == 'function' and 'f' or sym.type == 'object' and 'o' or '',
				SYM_COLUMN_SECT, sym.section,
				SYM_COLUMN_MEMOFS, "%08x" % sym.memofs)
			i = i + 1
		return lstore

	def __add_columns(self, treeview):
		model = treeview.get_model()
		
		namerenderer = gtk.CellRendererText()
		column = gtk.TreeViewColumn('Name', namerenderer,
		                            text=SYM_COLUMN_NAME, markup=1)
		column.set_sort_column_id(SYM_COLUMN_NAME)
		#column.set_attributes(namerenderer, markup=1)
		treeview.append_column(column)
		
		column = gtk.TreeViewColumn('T', gtk.CellRendererText(),
		                            text=SYM_COLUMN_TYPE, markup=0)
		column.set_sort_column_id(SYM_COLUMN_TYPE)
		treeview.append_column(column)
		
		column = gtk.TreeViewColumn('Section', gtk.CellRendererText(),
		                            text=SYM_COLUMN_SECT)
		column.set_sort_column_id(SYM_COLUMN_SECT)
		treeview.append_column(column)

		column = gtk.TreeViewColumn('Mem Offset', gtk.CellRendererText(),
		                             text=SYM_COLUMN_MEMOFS)
		column.set_sort_column_id(SYM_COLUMN_MEMOFS)
		treeview.append_column(column)
