import sys

import gobject
import gtk

import file.file
import ui.gtk


ui_info = \
'''<ui>
  <menubar name='MenuBar'>
    <menu action='FileMenu'>
      <menuitem action='Open'/>
      <separator/>
      <menuitem action='Quit'/>
    </menu>
    <menu action='ViewMenu'>
      <menuitem action='Sections'/>
      <menuitem action='Symbols'/>
    </menu>
    <menu action='HelpMenu'>
      <menuitem action='About'/>
    </menu>
  </menubar>
  <toolbar  name='ToolBar'>
    <toolitem action='Open'/>
  </toolbar>
</ui>'''


class main_window (gtk.Window):
	def __init__(self, f, dclass):
		# Create the toplevel window
		gtk.Window.__init__(self)
		self.connect('destroy', lambda *w: gtk.main_quit())

		self.file = f
		self.dclass = dclass

		self.update_title()
		self.set_default_size(700, 500)

		merge = gtk.UIManager()
		self.set_data("ui-manager", merge)
		merge.insert_action_group(self.__create_action_group(), 0)
		self.add_accel_group(merge.get_accel_group())

		try:
			mergeid = merge.add_ui_from_string(ui_info)
		except gobject.GError, msg:
			print "building menus failed: %s" % msg
		bar = merge.get_widget("/MenuBar")
		bar.show()

		table = gtk.Table(1, 4, False)
		self.add(table)

		table.attach(bar,
			# X direction #          # Y direction
			0, 1,                      0, 1,
			gtk.EXPAND | gtk.FILL,     0,
			0,                         0);

		bar = merge.get_widget("/ToolBar")
		bar.set_tooltips(True)
		bar.set_style(gtk.TOOLBAR_ICONS)
		bar.show()
		table.attach(bar,
			# X direction #       # Y direction
			0, 1,                   1, 2,
			gtk.EXPAND | gtk.FILL,  0,
			0,                      0)

		# Create document
		sw = gtk.ScrolledWindow()
		sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		sw.set_shadow_type(gtk.SHADOW_IN)
		self.sw = sw

		table.attach(sw,
			# X direction           Y direction
			0, 1,                   2, 3,
			gtk.EXPAND | gtk.FILL,  gtk.EXPAND | gtk.FILL,
			0,                      0)

		# Create statusbar
		self.statusbar = gtk.Statusbar()
		table.attach(self.statusbar,
			# X direction           Y direction
			0, 1,                   3, 4,
			gtk.EXPAND | gtk.FILL,  0,
			0,                      0)

		self.show_all()

		if (self.file):
			self.opened_file()


	def __create_action_group(self):
		# name, stock id, label, accelerator, tooltip

		# GtkActionEntry
		entries = (
			( "FileMenu", None, "_File" ),               # name, stock id, label
			( "ViewMenu", None, "_View" ),
			( "PreferencesMenu", None, "_Preferences" ), # name, stock id, label
			( "HelpMenu", None, "_Help" ),               # name, stock id, label
			( "Open", gtk.STOCK_OPEN,                    # name, stock id
			  "_Open","<control>O",                      # label, accelerator
			  "Open a file",                             # tooltip
			  self.a_open ),
			( "Quit", gtk.STOCK_QUIT,                    # name, stock id
			  "_Quit", "<control>Q",                     # label, accelerator
			  "Quit",                                    # tooltip
			  self.a_quit ),
			( "About", None,                             # name, stock id
			  "_About", "<control>A",                    # label, accelerator
			  "About",                                   # tooltip
			  self.a_about ),
			( "Sections", None,
			  "_Sections", "<control>S",
			  "Sections list",
			  self.a_sections ),
			( "Symbols", None,
			  "S_ymbols", "<control>Y",
			  "Symbols list",
			  self.a_symbols ),
		);

		# Create the menubar and toolbar
		action_group = gtk.ActionGroup("main_window_actions")
		action_group.add_actions(entries)

		return action_group

	def update_title(self):
		if (self.file):
			self.set_title('IDI - ' + self.file.name)
		else:
			self.set_title('IDI')

	def opened_file(self):
		assert(self.file)
		self.update_title()

		self.disas = self.dclass(self.file)
		self.disas.sweep()

		self.mapview = ui.gtk.mapview(self.file.map)
		self.sw.add(self.mapview)
		self.show_all()

	def a_open(self, action):
		dialog = gtk.FileChooserDialog()
		dialog.set_local_only(True)
		dialog.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL);
		dialog.add_button(gtk.STOCK_OPEN, gtk.RESPONSE_OK);
		res = dialog.run()
		if res == gtk.RESPONSE_OK:
			self.file = file.file.file(dialog.get_filename())
			self.opened_file()
		dialog.destroy()

	def a_about(self, action):
		dialog = gtk.AboutDialog()
		dialog.set_name("IDI - Interactive Disassembler")
		dialog.set_copyright("\302\251 Copyright 2007 Petr Baudis (SUSE Labs)")
		dialog.set_website("http://repo.or.cz/w/idi.git/")
		## Close dialog on user response
		dialog.connect ("response", lambda d, r: d.destroy())
		dialog.show()

	def a_quit(self, action):
		gtk.main_quit()

	def a_sections(self, action):
		dialog = ui.gtk.sectview(self);
		dialog.show()

	def a_symbols(self, action):
		dialog = ui.gtk.symview(self);
		dialog.show()


def main(dclass):
	if (len(sys.argv) < 2):
		f = None
	else:
		f = file.file.file(sys.argv[1])
	main_window(f, dclass)
	gtk.main()
