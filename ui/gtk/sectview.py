import gobject
import gtk


(
	SECT_COLUMN_IDX,
	SECT_COLUMN_NAME,
	SECT_COLUMN_MEMOFS,
	SECT_COLUMN_PHYSOFS,
	SECT_COLUMN_SIZE
) = range(5)


class sectview (gtk.Window):
	def __init__(self, mainwin):
		gtk.Window.__init__(self)
		self.main = mainwin
		self.set_screen(mainwin.get_screen())
		self.set_title('IDI: Sections')

		self.set_default_size(500, 300)

		sw = gtk.ScrolledWindow()
		sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
		sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
		self.add(sw)

		model = self.__create_model()

		treeview = gtk.TreeView(model)
		treeview.set_rules_hint(True)
		treeview.set_search_column(SECT_COLUMN_NAME)

		sw.add(treeview)
		self.__add_columns(treeview)
		self.show_all()

	def __create_model(self):
		# The offset and size columns should have numbers and
		# be formatted only when being rendered, however I didn't
		# figure out how to actually bend set_cell_data_func()
		# over to do that (I keep getting some internal GTK
		# assertion failures). So we pretend they are strings and
		# format them when inserting them into store...
		lstore = gtk.ListStore(
			gobject.TYPE_UINT,
			gobject.TYPE_STRING,
			gobject.TYPE_STRING,
			gobject.TYPE_STRING,
			gobject.TYPE_STRING)
			#gobject.TYPE_UINT64,
			#gobject.TYPE_UINT64,
			#gobject.TYPE_UINT64)

		i = 1
		for sect in self.main.file.sections:
			iter = lstore.append()
			lstore.set(iter,
				SECT_COLUMN_IDX, i,
				SECT_COLUMN_NAME, sect.name,
				SECT_COLUMN_MEMOFS, "%08x" % sect.memofs,
				SECT_COLUMN_PHYSOFS, "%08x" % sect.physofs,
				SECT_COLUMN_SIZE, "%06x" % sect.size)
			i = i + 1
		return lstore

	def __add_columns(self, treeview):
		model = treeview.get_model()

		def show_in_hex(treeviewcolumn, cell_renderer, model, iter, data):
			(col_id, width) = data
			num = model.get_value(iter, col_id)
			cell_renderer.set_property('text', ('%%0%dx' % width) % num)
			return

		column = gtk.TreeViewColumn('Idx', gtk.CellRendererText(),
		                            text=SECT_COLUMN_IDX)
		column.set_sort_column_id(SECT_COLUMN_IDX)
		treeview.append_column(column)
		
		column = gtk.TreeViewColumn('Name', gtk.CellRendererText(),
		                            text=SECT_COLUMN_NAME)
		column.set_sort_column_id(SECT_COLUMN_NAME)
		treeview.append_column(column)

		column = gtk.TreeViewColumn('Mem Offset', gtk.CellRendererText(),
		                             text=SECT_COLUMN_MEMOFS)
		column.set_sort_column_id(SECT_COLUMN_MEMOFS)
		#column.set_cell_data_func(gtk.CellRendererText(), show_in_hex, (SECT_COLUMN_MEMOFS, 8))
		treeview.append_column(column)

		column = gtk.TreeViewColumn('File Offset', gtk.CellRendererText(),
		                             text=SECT_COLUMN_PHYSOFS)
		column.set_sort_column_id(SECT_COLUMN_PHYSOFS)
		#column.set_cell_data_func(gtk.CellRendererText(), show_in_hex, (SECT_COLUMN_PHYSOFS, 8))
		treeview.append_column(column)

		column = gtk.TreeViewColumn('Size', gtk.CellRendererText(),
		                             text=SECT_COLUMN_SIZE)
		column.set_sort_column_id(SECT_COLUMN_SIZE)
		#column.set_cell_data_func(gtk.CellRendererText(), show_in_hex, (SECT_COLUMN_SIZE, 4))
		treeview.append_column(column)
