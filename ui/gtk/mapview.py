import gobject
import gtk


def _trim(string, length):
	return len(string) > length and (string[:(length - 4)].rstrip() + ' ...') or string


class mapview (gtk.TextView):
	def __init__(self, m):
		gtk.TextView.__init__(self)

		self.set_wrap_mode(gtk.WRAP_NONE)
		self.set_editable(True)

		self.__create_tags(self.get_buffer())

		self.__map = m
		self.map2buffer(self.get_buffer())

	def __create_tags(self, buf):
		buf.create_tag("monospace", family="monospace")
		buf.create_tag("offset", foreground="lightblue")
		buf.create_tag("dummy", foreground="darkred")
		buf.create_tag("code", foreground="black")
		buf.create_tag("data", foreground="darkgreen")
		buf.create_tag("comment", foreground="darkgray")


	def __display_dummy(self, iter, seg):
		self.get_buffer().insert_with_tags_by_name(iter, seg.name, 'dummy')

	def __display_code(self, iter, seg):
		self.get_buffer().insert_with_tags_by_name(iter, seg.txt, 'code')

	def __display_data(self, iter, seg):
		data = self.__map.file.data(seg.physofs, seg.len)
		strs = seg.string(data)
		self.get_buffer().insert_with_tags_by_name(iter, '%-2d  %s' % (seg.cellsize, _trim(', '.join(strs), 72)), 'data')

	__displayers = { 'seg_code': __display_code, 'seg_data': __display_data, 'seg_comment': __display_dummy }


	def map2buffer(self, buf):
		iter = buf.get_iter_at_offset(0)

		for seg in self.__map.segments:
			if seg.__class__.__name__ == 'seg_comment':
				buf.insert_with_tags_by_name(iter, "%04x\n" % seg.memofs, 'offset')
				buf.insert_with_tags_by_name(iter, "%04x\t" % seg.memofs, 'offset')
				buf.insert_with_tags_by_name(iter, "# %s\n" % seg.comment, 'comment')
				buf.insert_with_tags_by_name(iter, "%04x\n" % seg.memofs, 'offset')
			else:
				buf.insert_with_tags_by_name(iter, "%04x\t" % seg.memofs, 'offset')
				if seg.__class__.__name__ in self.__displayers:
					self.__displayers[seg.__class__.__name__](self, iter, seg)
				else:
					self.__display_dummy(iter, seg)
				if seg.comment:
					buf.insert_with_tags_by_name(iter, "\t# %s" % seg.comment, 'comment')
				buf.insert(iter, "\n")

		buf.apply_tag_by_name("monospace", buf.get_start_iter(), buf.get_end_iter())
