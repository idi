import disasm
import file.file
import file.map

class dummy (disasm.disasm):
	"The dummy disassembler"
	# This is a "disassembler" that just takes the whole file
	# as data. ;-)

	# Sweep the whole map and disassemble code segments
	def sweep(self):
		map = self.file.map
		map.new_segment(file.map.seg_data(map, 0, self.file.size))
