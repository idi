from subprocess import *
import re
import sys

import disasm
import file.file
import file.map
import file.section
import file.symbol

class objdump (disasm.disasm):
	"The objdump-based disassembler"
	# This disassembler engine calls objdump as the backend.

	# Sweep the whole map and disassemble code segments
	def sweep(self):
		map = self.file.map
		p = Popen(["objdump", "-Tfdx", self.file.name], stdout=PIPE).stdout
		state = None
		cursor = long(0)
		physcursor = long(0)
		lastseg = None
		codesegs = []

		# XXX: Mem-to-phys offset is currently hardcoded
		#base = 0x8048000 # XXX, i386
		base = 0x400000 # XXX, x86_64

		for line in p:
			line = line.splitlines()[0]
			#print "l %s" % line;

			# Special state
			if (state == 'sections'):
				state = self._section_line(line)
				if state:
					continue
			if (state == 'symbols'):
				state = self._symbol_line(line)
				if state:
					continue

			if (line == "Sections:"):
				state = 'sections'
				continue
			if (line == "SYMBOL TABLE:") or (line == "DYNAMIC SYMBOL TABLE:"):
				state = 'symbols'
				continue

			#  8048d15:       c7 44 24 04 01 00 00    movl   $0x1,0x4(%esp)
			m = re.match(r"^  *([0-9a-f]+):\t(.*? ) *\t(.*?)([ \t]*# (.*))?$", line)
			if (m):
				# Instruction
				ofs = long(m.group(1), 16)
				physofs = ofs - base
				#sys.stderr.write("of %x le %x\n" % (ofs, cursor));

				# Fill gap with data
				assert(ofs >= cursor)
				if (ofs > cursor):
					map.new_segment(file.map.seg_data(map, cursor, physcursor, ofs - cursor))
				for seg in codesegs:
					seg.physofs = ofs
					seg.memofs = ofs
					map.new_segment(seg)
				codesegs = []

				# Find out length
				bytes = m.group(2)
				length = len(bytes) / 3
				#sys.stderr.write("b: %s l %d\n" % (bytes, length));

				txt = m.group(3)
				#sys.stderr.write(".%s.(%s)\n" % (m.group(3), m.group(4)))

				lastseg = file.map.seg_code(map, ofs, physofs, length, m.group(5), txt)
				cursor = ofs + length
				physcursor = physofs + length
				map.new_segment(lastseg)
				continue

			#  8048d1c:       00
			m = re.match(r"^  *([0-9a-f]+):\t(.* ) *$", line);
			if (m):
				# Instruction continuation
				ofs = long(m.group(1), 16)
				physofs = ofs - base
				#sys.stderr.write("of %x le %x\n" % (ofs, cursor));
				assert(ofs == cursor)

				# Find out length
				bytes = m.group(2)
				length = len(bytes) / 3

				lastseg.len += length
				cursor += length
				physcursor += length
				map.new_segment(lastseg)
				continue

			# Disassembly of section .init:
			m = re.match(r"^Disassembly of section (.*):", line);
			if (m):
				#sys.stderr.write("<%s>\n" % m.group(1))
				codesegs.append(file.map.seg_comment(map, -1, -1, 0, '====== SECTION %s ======' % m.group(1)))
				continue

			# 0000000000400f98 <getopt_long@plt>:
			m = re.match(r"^([0-9a-f]+) <(.*?)>:$", line)
			if (m):
				#sys.stderr.write("<%s>\n" % m.group(2))
				codesegs.append(file.map.seg_comment(map, -1, -1, 0, ' Symbol %s' % m.group(2)))
				continue


		# Trailing data
		if (cursor < self.file.size):
			map.new_segment(file.map.seg_data(map, cursor, physcursor, self.file.size - physcursor))


	# objdump output state machine helpers:
	def _section_line(self, line):
		#print 'l: %s?' % line
		# Idx Name          Size      VMA               LMA               File off  Algn
		if re.match(r"^Idx ", line):
			return 'sections'

		#  0 .interp       0000001c  0000000000400238  0000000000400238  00000238  2**0
		m = re.match(r"^ *(\d+) ([a-zA-Z0-9_.-]+) +([0-9a-f]+) +([0-9a-f]+) +([0-9a-f]+) +([0-9a-f]+) +(.*)$", line)
		if m:
			size = long(m.group(3), 16)
			memofs = long(m.group(4), 16)
			physofs = long(m.group(6), 16)
			self.__lastsect = file.section.section(self.file, m.group(2), memofs, physofs, size, {})
			self.file.new_section(self.__lastsect)
			return 'sections'

		#                  CONTENTS, ALLOC, LOAD, READONLY, DATA
		m = re.match(r"^                 (.*)", line)
		if m:
			# TODO: Set flags on self.__lastsect
			return 'sections'

		return None

	def _symbol_line(self, line):
		# VALUE--- FLAGS-- SECTION\t    OTHERVAL  VERSION---- NAME
		# 08049aea g     F .text	00000000              .hidden __i686.get_pc_thunk.bx
		# 08049b1c g     F .fini	00000000              _fini
		# 08049b3c g     O .rodata	00000004              _IO_stdin_used
		# 00000000       F *UND*	000001ca              fclose@@GLIBC_2.1
		# 00000000  w      *UND*	00000000              _Jv_RegisterClasses
		# 08049cc8 l     O .eh_frame	00000000              __FRAME_END__
		# 00000000 l    df *ABS*	00000000              mplex.c
		# 00002810 g    DF .text	0000009d  Base        gst_tag_from_vorbis_tag
		# 00000000      DF *UND*	00000049  GLIBC_2.0   dcgettext
		m = re.match(r"^([0-9a-f]+) (.)(.)(.)(.)(.)(.)(.) (.*?)\t([0-9a-f]+)  (.*?) +([^ ]+)$", line)
		if not m:
			return None

		value = long(m.group(1), 16)
		scope = m.group(2) == 'g' and 'global' or m.group(2) == 'l' and 'local' or None
		weak = m.group(3) == 'w'
		constructor = m.group(4) == 'C'
		warning = m.group(5) == 'W'
		indirect = m.group(6) == 'I'
		debugging = m.group(7) == 'd'
		dynamic = m.group(7) == 'D'
		type = m.group(8) == 'F' and 'function' or m.group(8) == 'f' and 'file' or m.group(8) == 'O' and 'object' or None
		section = m.group(9)
		if section == '*ABS*':
			section = None
			absolute = 1
		elif section == '*UND*':
			section = None
			undefined = 1
		otherval = long(m.group(10), 16)
		version = m.group(11)
		name = m.group(12)

		# Flags:
		# fprintf (file, " %c%c%c%c%c%c%c",
		#          ((type & BSF_LOCAL)
		#           ? (type & BSF_GLOBAL) ? '!' : 'l'
		#           : (type & BSF_GLOBAL) ? 'g' : ' '),
		#          (type & BSF_WEAK) ? 'w' : ' ',
		#          (type & BSF_CONSTRUCTOR) ? 'C' : ' ',
		#          (type & BSF_WARNING) ? 'W' : ' ',
		#          (type & BSF_INDIRECT) ? 'I' : ' ',
		#          (type & BSF_DEBUGGING) ? 'd' : (type & BSF_DYNAMIC) ? 'D' : ' ',
		#          ((type & BSF_FUNCTION)
		#           ? 'F'
		#           : ((type & BSF_FILE)
		#              ? 'f'
		#              : ((type & BSF_OBJECT) ? 'O' : ' '))));
		#
		# Anything after flags is format-specific. :(
		#
		# About VALUE and OTHERVAL:
		#         /* Print the "other" value for a symbol.  For common symbols,
		#            we've already printed the size; now print the alignment.
		#            For other symbols, we have no specified alignment, and
		#            we've printed the address; now print the size.  */
		# And before name can be...:
		#	switch (st_other)
		#	  {
		#	  case 0: break;
		#	  case STV_INTERNAL:  fprintf (file, " .internal");  break;
		#	  case STV_HIDDEN:    fprintf (file, " .hidden");    break;
		#	  case STV_PROTECTED: fprintf (file, " .protected"); break;
		#	  default:
		#	    /* Some other non-defined flags are also present, so print
		#	       everything hex.  */
		#	    fprintf (file, " 0x%02x", (unsigned int) st_other);
		#	  }
		# -- http://sourceware.org/cgi-bin/cvsweb.cgi/src/bfd/elf.c?rev=1.394&content-type=text/x-cvsweb-markup&cvsroot=src

		# We drastically prune the symbol list. It is all quite
		# complicated (even more complicated if we allow debugging
		# symbols), but we care only about symbols with:
		#	1) Defined section
		#	2) Not debugging
		#	3) Not file
		# We ignore the 'other value' field.
		# 'version' field is currently also ignored. And 'scope'.

		if not section:
			return 'symbols'
		if debugging:
			return 'symbols'
		if type == 'file':
			return 'symbols'

		#print "<%s, %x, %s, %s>" % (name, value, section, type)
		self.file.new_symbol(file.symbol.symbol(self.file, name, value, section, type))

		return 'symbols'
