class disasm:
	"The disassembler interface"
	# This class describes generic interface for disassembly
	# engines. However, we don't just want a single full
	# disassembly from the engine, but we also need to tell
	# it to re-disassemble something based on user input.
	# The user says what is code and what isn't, and the
	# disassembler might use previous choices for hinting.
	#
	# The disassembler plays upon file map (file.map.map). When
	# it is told to sweep (only once), it also scans the
	# symbol tables and generic file information, and makes
	# initial guess on what the code is.
	#
	# If user changes status of a map segment to/from code
	# (or creates new such segment), disassembler is called
	# back with the segment id given and it is expected to
	# update the disassembly of the segment (and possibly
	# other, nearby or cross-referenced bits) appropriately.
	def __init__(self, file):
		self.file = file

	# Sweep the whole map and disassemble code segments
	def sweep(self):
		pass

	# Given code segments just appeared (new or
	# from data) (segments [class file.map.segment])
	def new_code(self, segments):
		pass

	# Private attributes:
	# file (class file.file.file)
