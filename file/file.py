import os
import map
from stat import *

class file:
	# This class represents the file being disassembled interactively.
	def __init__(self, filename):
		self.name = filename
		self.map = map.map(self)
		self.sections = []
		self.symbols = {}

		self.handle = open(self.name, "rb")
		self.size = os.stat(self.name).st_size

	# Get raw data from file.
	# TODO: Caching, or just preload the whole file to memory?
	def data(self, ofs, len):
		self.handle.seek(ofs)
		return self.handle.read(len)

	# Add section.
	def new_section(self, section):
		self.sections.append(section)

	# Add symbol
	def new_symbol(self, symbol):
		self.symbols[symbol.name] = symbol
	
	# Public attributes:
	# name (string)
	# map (class file.map.map)
	# sections ([class file.section])
	# symbols ({string(name): class file.symbol})
	# size (int)
	# handle (filehandle)
