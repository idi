from bisect import bisect
from struct import unpack
from array import array

class map:
	"Object map"
	# This is the core idi's data structure, mapping the file and
	# slicing it to many "segments" of various types.
	#
	# Note that the segments may overlap. Also note that this is
	# TOTALLY different from executable format's notion of "segments".
	# Our segments are actually very tiny, typically spanning one
	# data item or a single instruction.
	def __init__(self, file):
		self.file = file
		self.segments = []

	# Add new segment to the map.
	def new_segment(self, seg):
		# Optimize for common case - appending
		pos = len(self.segments)
		if (seg.physofs < pos):
			pos = bisect(self.segments, seg.physofs);
		self.segments.insert(pos, seg)

	# Public attributes:
	# file (class file.file.file)
	# segments ([class segment]) (ordered by offset)


class segment:
	"Object map segment"
	# This is one segment in the file; that is, continuous part of the
	# file that is either code or data. There can be also zero-sized
	# comment-only segments.
	def __init__(self, map, memofs, physofs, len, comment = None):
		self.map = map
		self.memofs = memofs
		self.physofs = physofs
		self.len = len
		self.comment = comment

	# Public attributes:
	# map (class map)
	# memofs (int, offset in memory)
	# physofs (int, offset in file)
	# len (int)
	# comment (string, can be None)
	# name (string)
	#	Contains user-visible name of segment type; use __class__ for
	#	programmatic segment type inspection instead.
	name = 'Generic'


# XXX: I don't quite like how comments are done now.


class seg_code (segment):
	"Map segment - code instruction"
	# Code segment: single instruction
	#
	# TODO: This object is very rudimentary now, it will probably get
	# much richer over time - flow information for jump instructions,
	# register information, etc.
	#
	# len is length of the instruction's binary representation in octets
	# txt is the textual representation of the instruction
	#	(TODO: This should be richer - registers identified, etc.)
	# comment is optional comment, usually added by the user
	def __init__(self, map, memofs, physofs, len, comment, txt):
		segment.__init__(self, map, memofs, physofs, len, comment)
		self.txt = txt

	# Public attributes:
	# txt (string)
	name = 'Code'


# TODO: More FMT support
FMT_HEX=0	# cellsize 0: whole segment in hex
#FMT_DEC=1	# cellsize 0: N/A
#FMT_OCT=2	# cellsize 0: whole segment in hex
#FMT_FLOAT=3	# cellsize 0: N/A
#FMT_ASCII=4	# cellsize 0: whole segment in quoted ascii
#FMT_ZASCII=4	# cellsize!0: up to \0
#FMT_PASCII=5	# cellsize!0: first char is len
# ...

# Cell formatters:

def _fmt_1hex(num):
	return "%02x" % num

def _fmt_hex(data):
	return reduce(lambda a, b: a + _fmt_1hex(b) + ' ', array("B", data), '')

_fmts = [_fmt_hex];

class seg_data (segment):
	"Map segment - data"
	# Data segment
	# Consists of one or more cells of given size, formatted in
	# given way

	# Convert given data to a string (assuming the data is contents
	# of this segment); returns array of strings, one per cell
	def string(self, data):
		if self.cellsize > 0:
			return filter(_fmts[self.format],
					[data[i:i + self.cellsize]
						for i in range(0, len(data), self.cellsize)])
		else:
			return [_fmts[self.format](data)]

	# Public attributes:
	# format (int: DATA_*)
	# cellsize (int; 0 is fmt specific)
	# TODO: endianity
	format = FMT_HEX
	cellsize = 0

	name = 'Data'


class seg_comment (segment):
	"Map segment - comment"
	# Comment (that is not bound with any code/data)

	name = 'Comment'
