class symbol:
	# This class represents a symbol within the file.
	def __init__(self, file, name, memofs, section, type):
		self.file = file
		self.name = name
		self.memofs = memofs
		self.section = section
		self.type = type

	# Public attributes:
	# file (class file.file)
	# name (string)
	# memofs (int)
	# section (string)
	# type ('function', 'object')
