class section:
	# This class represents a file section (in the binary file format's
	# sense: like, ELF section, etc).
	def __init__(self, file, name, memofs, physofs, size, flags):
		self.file = file
		self.name = name
		self.memofs = memofs
		self.physofs = physofs
		self.size = size
		self.flags = flags

	# Public attributes:
	# file (class file.file)
	# name (string)
	# memofs (int)
	# physofs (int)
	# size (int)
	# flags ({string: True})
